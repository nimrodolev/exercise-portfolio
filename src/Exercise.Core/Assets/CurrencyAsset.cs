using System;
using Exercise.Abstractions;

namespace Exercise.Core
{
    public class CurrencyAsset : IAsset
    {
        public CurrencyAsset(CurrencyCode currencyCode, double amount)
        {
            Amount = amount;
            CurrencyCode = currencyCode;
        }

        public double Amount { get; }
        public CurrencyCode CurrencyCode { get; }

        public IAsset Consolidate(IAsset other)
        {
            CurrencyAsset o = ValidateBeforeConsolidation(other);
            return new CurrencyAsset(this.CurrencyCode, this.Amount + o.Amount);
        }

        public string GetConsolidationKey()
        {
            return $"{nameof(CurrencyAsset)}/{this.CurrencyCode}";
        }

        public Money GetValue()
        {
            return new Money
            {
                Value = Amount,
                CurrencyCode = CurrencyCode
            };
        }

        private CurrencyAsset ValidateBeforeConsolidation(IAsset other)
        {
            CurrencyAsset o = null;
            if ((o = other as CurrencyAsset) == null)
                throw new NotSupportedException($"Can not consolidate asset types {typeof(CurrencyAsset).Name} and {other.GetType().Name}");
            
            if (o.CurrencyCode != this.CurrencyCode)
                throw new NotSupportedException($"Can not consolidate asset with different currencies [{this.CurrencyCode.ToString()}/{o.CurrencyCode.ToString()}]");
            return o;
        }
    }
}