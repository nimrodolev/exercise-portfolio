using System;
using Exercise.Abstractions;


namespace Exercise.Core
{
    public class StockAsset : IAsset
    {
        public StockAsset(string symbol, double shares, double price, CurrencyCode currencyCode)
        {
            Symbol = symbol;
            Shares = shares;
            Price = price;
            CurrencyCode = currencyCode;
        }

        public string Symbol { get; }
        public double Shares { get; }
        public double Price { get; }
        public CurrencyCode CurrencyCode { get; }

        public IAsset Consolidate(IAsset other)
        {
            StockAsset o = ValidateBeforeConsolidation(other);
            var shareSum = this.Shares + o.Shares;
            var valueSum = this.GetValue().Value + other.GetValue().Value;
            return new StockAsset(this.Symbol, shareSum, valueSum / shareSum, this.CurrencyCode);
        }

        public string GetConsolidationKey()
        {
            return $"{nameof(StockAsset)}/{this.Symbol}/{this.CurrencyCode}";
        }

        public Money GetValue()
        {
            return new Money
            {
                Value = Shares * Price,
                CurrencyCode = CurrencyCode
            };
        }

        private StockAsset ValidateBeforeConsolidation(IAsset other)
        {
            StockAsset o = null;
            if ((o = other as StockAsset) == null)
                throw new NotSupportedException($"Can not consolidate asset types {typeof(StockAsset).Name} and {other.GetType().Name}");
            if (o.Symbol != this.Symbol)
                throw new NotSupportedException($"Can not consolidate asset with different symbol [{this.Symbol}/{o.Symbol}]");
            if (o.CurrencyCode != this.CurrencyCode)
                throw new NotSupportedException($"Can not consolidate asset with different currencies [{this.CurrencyCode.ToString()}/{o.CurrencyCode.ToString()}]");
                
            return o;
        }
    }
}