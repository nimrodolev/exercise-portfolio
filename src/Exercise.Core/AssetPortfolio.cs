using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Exercise.Abstractions;

namespace Exercise.Core
{
    public class AssetPortfolio
    {
        public List<IAsset> Portfolio { get; set; } = new List<IAsset>();

        private IExchangeRates _exchangeService;


        public AssetPortfolio(IExchangeRates exchangeService, IEnumerable<IAsset> assets)
        {
            _exchangeService = exchangeService;
            foreach (var asset in assets)
                Portfolio.Add(asset);
        }
        
        public AssetPortfolio(IExchangeRates exchangeService)
        {
            _exchangeService = exchangeService;
        }

        public void Add(IAsset s)
        {
            Portfolio.Add(s);
        }

        public async Task<double> CalculateValueAsync(CurrencyCode currencyCode)
        {
            double sum = 0;
            foreach (var s in Portfolio)
            {
                var amount = s.GetValue();
                var value = amount.Value;
                if (amount.CurrencyCode != currencyCode)
                {
                    var rate = await  _exchangeService.GetRateAsync(amount.CurrencyCode, currencyCode);
                    value = value * rate;
                }
                sum += value;
            }
            return sum;
        }

        public AssetPortfolio Consolidate()
        {
            var consolidationMap = new Dictionary<string, IAsset>();
            foreach (var asset in this.Portfolio)
            {
                var consolidationKey = asset.GetConsolidationKey();
                if (!consolidationMap.ContainsKey(consolidationKey))
                    consolidationMap[consolidationKey] = asset;
                else
                    consolidationMap[consolidationKey] = consolidationMap[consolidationKey].Consolidate(asset); 
            }

            return new AssetPortfolio(this._exchangeService, consolidationMap.Values);
        }
    }
}