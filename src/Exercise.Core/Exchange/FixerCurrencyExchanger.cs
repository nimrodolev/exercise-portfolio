using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using Exercise.Abstractions;
using System.Net.Http;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Exercise.Core
{
    public class FixerCurrencyExchanger : IExchangeRates
    {
        private const string API_KEY = "366d5a4a02f0bcb59d5f6e6b61f449a8";
        private const string API_ENDPOINT = "http://data.fixer.io/api/latest?access_key=";
        private const int MAX_AGE_MILLISECONDS = 1000 *60 * 60; //let's refresh every hour

        private DateTime _lastRefresh = DateTime.MinValue;
        private IDictionary<CurrencyCode, double> _rates;
        private SemaphoreSlim _lock = new SemaphoreSlim(1, 1);

        public async Task<double> GetRateAsync(CurrencyCode from, CurrencyCode to)
        {
            await RefreshRatesIfNeededAsync();
            var fromToEUR = _rates[from];
            var toToEUR = _rates[to];
            return toToEUR / fromToEUR;
        }

        private async Task<IDictionary<CurrencyCode, double>> GetRatesAsync()
        {
            FixerRatesResponse res = null;
            var client = new HttpClient();
            var respStream = await client.GetStreamAsync($"{API_ENDPOINT}{API_KEY}");
            using (var reader = new StreamReader(respStream))
            using (var jsonReader = new JsonTextReader(reader))
            {
                var ser = JsonSerializer.Create();
                ser.Converters.Add(new StringEnumConverter());
                res = ser.Deserialize<FixerRatesResponse>(jsonReader);
            }
            if (!res.Success || res.Base != CurrencyCode.EUR || res.Rates == null || res.Rates.Count == 0)
                throw new Exception("Error pulling rates from fixer API");
            return res.Rates;
        }

        private async Task RefreshRatesIfNeededAsync()
        {
            if (ShouldPullRates())
            {
                await _lock.WaitAsync();
                try
                {
                    _rates = await GetRatesAsync();
                    _lastRefresh = DateTime.Now;
                }
                finally
                {
                    _lock.Release();
                }
            }
        }

        private bool ShouldPullRates()
        {
            return _rates == null || (DateTime.Now - _lastRefresh).TotalMilliseconds > MAX_AGE_MILLISECONDS;
        }

        class FixerRatesResponse
        {
            public bool Success { get; set; }
            public CurrencyCode Base { get; set; }
            public Dictionary<CurrencyCode, double> Rates {get; set; }
        }
    }
}