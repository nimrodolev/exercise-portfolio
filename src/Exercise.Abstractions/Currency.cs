namespace Exercise.Abstractions
{
    public class Money
    {
        public double Value { get; set; }
        public CurrencyCode CurrencyCode { get; set; }
    }
}