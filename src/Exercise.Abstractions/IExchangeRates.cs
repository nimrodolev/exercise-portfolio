using System.Threading.Tasks;

namespace Exercise.Abstractions
{
    public interface IExchangeRates
    {
        Task<double> GetRateAsync(CurrencyCode from, CurrencyCode to);
    }
}