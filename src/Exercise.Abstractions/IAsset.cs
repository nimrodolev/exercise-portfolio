using System;

namespace Exercise.Abstractions
{
    public interface IAsset
    {
        Money GetValue();
        string GetConsolidationKey();
        IAsset Consolidate(IAsset other);
    }
}