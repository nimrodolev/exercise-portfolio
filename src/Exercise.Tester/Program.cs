﻿using System;
using System.Threading.Tasks;
using Exercise.Abstractions;
using Exercise.Core;

namespace Exercise.Tester
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Test1_Just_What_Came_With_The_Exercise();
            Test2_Make_Sure_Consolidation_Sums_Correctly();
            Test3_Make_Sure_Consolidation_Keeps_Different_Currencies_Separate();
            Test4_Make_Sure_Conversion_Is_Correct();

            Console.WriteLine("All test done... (Press a key to close)");
            Console.ReadKey();
        }

        private static void Test1_Just_What_Came_With_The_Exercise()
        {
            Console.WriteLine($"Executing test '{nameof(Test1_Just_What_Came_With_The_Exercise)}'");
            var exchangeRateService = new FixerCurrencyExchanger();
            var portfolio = new AssetPortfolio(exchangeRateService);
            portfolio.Add(new StockAsset("ABC", 200, 4, CurrencyCode.EUR));
            portfolio.Add(new StockAsset("DDW", 100, 10, CurrencyCode.EUR));
            var shouldBe = 1800;
            var val = portfolio.CalculateValueAsync(CurrencyCode.EUR).Result;
            Assert(AreEqual(val, shouldBe), $"Test1 Failed, Expected Value:\t{shouldBe},\tActual Value: \t{val}\n");
        }

        private static void Test2_Make_Sure_Consolidation_Sums_Correctly()
        {
            Console.WriteLine($"Executing test '{nameof(Test2_Make_Sure_Consolidation_Sums_Correctly)}'");
            var exchangeRateService = new FixerCurrencyExchanger();
            var portfolio = new AssetPortfolio(exchangeRateService);
            portfolio.Add(new StockAsset("ABC", 200, 4, CurrencyCode.EUR));
            portfolio.Add(new StockAsset("ABC", 100, 2, CurrencyCode.EUR));
            portfolio.Add(new StockAsset("DDW", 100, 10, CurrencyCode.EUR));
            portfolio.Add(new StockAsset("DDW", 50, 5, CurrencyCode.EUR));
            portfolio = portfolio.Consolidate();                        
            var shouldBe = 2250;
            var val = portfolio.CalculateValueAsync(CurrencyCode.EUR).Result;
            Assert(AreEqual(val, shouldBe), $"Test2 Failed, Expected Value:\t{shouldBe},\tActual Value: \t{val}\n");
        }

        private static void Test3_Make_Sure_Consolidation_Keeps_Different_Currencies_Separate()
        {
            Console.WriteLine($"Executing test '{nameof(Test3_Make_Sure_Consolidation_Keeps_Different_Currencies_Separate)}'");
            var exchangeRateService = new FixerCurrencyExchanger();
            var portfolio = new AssetPortfolio(exchangeRateService);
            portfolio.Add(new StockAsset("ABC", 200, 4, CurrencyCode.EUR));
            portfolio.Add(new StockAsset("DDW", 100, 10, CurrencyCode.EUR));
            portfolio.Add(new StockAsset("DDW", 50, 5, CurrencyCode.USD));
            var shouldBe = portfolio.Portfolio.Count;
            portfolio = portfolio.Consolidate();                        
            var val = portfolio.Portfolio.Count;
            Assert(AreEqual(val, shouldBe), $"Test3 Failed, Expected Value:\t{shouldBe},\tActual Value: \t{val}\n");
        }

        private static void Test4_Make_Sure_Conversion_Is_Correct()
        {
            Console.WriteLine($"Do you want to run '{nameof(Test4_Make_Sure_Conversion_Is_Correct)}'?");
            Console.WriteLine("It will require some help from you (:");
            var answer = ReadYesNoAnswer();
            if (!answer)
                return;
            Console.WriteLine("Cool, lets run a test!");
            var exchangeRateService = new FixerCurrencyExchanger();
            var portfolio = new AssetPortfolio(exchangeRateService);
            portfolio.Add(new StockAsset("ABC", 100, 4, CurrencyCode.USD));
            portfolio.Add(new StockAsset("DDW", 50, 10, CurrencyCode.USD));
            var valueNoConversion = portfolio.CalculateValueAsync(CurrencyCode.USD).Result;
            var valueWithConversion = portfolio.CalculateValueAsync(CurrencyCode.CAD).Result;
            Console.WriteLine($"Now your part");
            Console.WriteLine($"Are {valueNoConversion} US Dollars (USD) worth about {valueWithConversion} Canadian Dollars (CAD) at the moment?");
            Console.WriteLine($"You can use this link to check - https://www.google.com/search?q={valueNoConversion}+usd+to+cad");
            Console.WriteLine("Do consider - we could be a tiny bit off because of conversion rate differences (:");
            answer = ReadYesNoAnswer();
            Assert(answer, $"Test4 Failed, because you said so");
        }

        private static bool ReadYesNoAnswer()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Please answer by clicking y/n");
            Console.ResetColor();
            try
            {
                while (true)
                {
                    var key = Console.ReadKey();
                    if (key.KeyChar == 'n')
                        return false;
                    if (key.KeyChar == 'y')
                        return true;
                    
                    Console.WriteLine();
                    Console.WriteLine("Sorry, I didn't get that (: Can you try again please?");
                }
            }
            finally
            {
                Console.WriteLine();
            }
        }

        private static void Assert(bool condition, string failure)
        {
            if (!condition)
                Console.WriteLine(failure);
        }

        private static bool AreEqual(double d1, double d2)
        {
            return Math.Abs(d1 - d2) < .0001;
        }
    }
}